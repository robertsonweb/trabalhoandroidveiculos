package com.example.veiculos;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PessoaCadActivity extends Activity {

	public EditText edtId, edtNome, edtLogin, edtSenha;
	public Button btnGravar, btnVoltar;
	public Intent itPrincipal;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pessoa_cad);
		
		//referenciar edtId
		this.edtId = (EditText) findViewById(R.id.edtId);
		
		//referenciar edtNome
		this.edtNome = (EditText) findViewById(R.id.edtNome);
		
		//referenciar edtLogin
		this.edtLogin = (EditText) findViewById(R.id.edtLogin);
		
		//referenciar edtLogin
		this.edtSenha = (EditText) findViewById(R.id.edtSenha);
		
		//referenciar btnGravar
		this.btnGravar = (Button) findViewById(R.id.btnGravar);
		
		//referenciar btnVoltar
		this.btnVoltar = (Button) findViewById(R.id.btnVoltar);
		
		//referencia Intent LoginActivity
		this.itPrincipal = new Intent(this, PrincipalActivity.class);
	}
	
	public void btnGravarClick(View v){
		String msg;
		
		if(this.edtNome.getText().toString()==""){
			msg = "Você deve preencher seu nome!";
		}else if(this.edtLogin.getText().toString()==""){
			msg = "Você deve preencher com o seu login!";
		}else if(this.edtSenha.getText().toString()==""){
			msg = "Você deve preencher sua senha!";
		}else{
			Pessoa pessoa = new Pessoa(getApplicationContext(),
										Integer.parseInt(this.edtId.getText().toString()),
										this.edtNome.getText().toString(),
										this.edtLogin.getText().toString(),
										this.edtSenha.getText().toString());
			
			if(pessoa.gravar()){
				msg = "Pessoa gravada com Sucesso!";
			}else{
				msg = "Erro ao gravar a Pessoa!";
			}
		}
		
		//feedback pro usuario
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}

	public void btnVoltarClick(View v){
		finish();
	}

}
