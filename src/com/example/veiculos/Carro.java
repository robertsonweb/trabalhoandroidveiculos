package com.example.veiculos;
import android.content.Context;

public class Carro {

	private int 	id;
	private String	modelo;
	private String 	marca;
	private String	ano;
	private float	preco;
	private int		quilometragem;
	private Pessoa	pessoa;
	private Context contexto;
	
	public Carro(Context contexto, int id, String modelo, String marca, String ano, float preco , int quilometragem, Pessoa pessoa) {
		this.contexto		= contexto;
		this.id 			= id;
		this.modelo			= modelo;
		this.marca			= marca;
		this.ano			= ano;
		this.preco			= preco;
		this.quilometragem	= quilometragem;
		this.pessoa			= pessoa;
	}
	
	public Boolean gravar(){
		Banco oBanco = new Banco(this.contexto);
		
		if(this.id==0){
			//insert
			return oBanco.executaSQL("INSERT INTO CARRO (ROWID, MODELO, MARCA, ANO, PRECO, QUILOMETRAGEM, ID_PESSOA) VALUES (null,'"+this.modelo+"','"+this.marca+"','"+this.ano+"',"+this.ano+","+this.preco+","+this.quilometragem+","+this.pessoa.getId()+")");			
		}else{
			//update
			return oBanco.executaSQL("UPDATE PESSOA SET MODELO='"+this.modelo+"', MARCA='"+this.marca+"', ANO='"+this.ano+"',"+this.preco+","+this.quilometragem+","+this.pessoa.getId()+" WHERE ID= "+this.id);
		}
	}

}
