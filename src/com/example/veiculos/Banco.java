package com.example.veiculos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;


public class Banco {

	//Objeto SQLiteDataBase para manipulação do banco
	private SQLiteDatabase db;
	
	private Context ctx;
	
	//String contendo o nome do banco (que na verdade é um arquivo)
	private String nome_banco = "banco.db";
	
	public Banco(Context ctx) {
		this.ctx = ctx;
		this.criarBanco();
	}
	
	public void criarBanco(){
		/*Abre ou cria a Database*/
	 	this.db = this.ctx.openOrCreateDatabase(this.nome_banco, Context.MODE_PRIVATE, null);
		
		/*Criar as tabelas, caso nao existirem*/
		this.db.execSQL("CREATE TABLE IF NOT EXISTS PESSOA(ID INTEGER PRIMARY KEY, NOME TEXT, LOGIN TEXT, SENHA TEXT);");	
		this.db.execSQL("CREATE TABLE IF NOT EXISTS CARRO(ID INTEGER PRIMARY KEY, MODELO TEXT, MARCA TEXT, ANO TEXT, PRECO DOUBLE, QUILOMETRAGEM DOUBLE, ID_PESSOA INTEGER, FOREIGN KEY (ID_PESSOA) REFERENCES PESSOA(ID));");
		
		this.db.close();
	}
	
	public void close(){
		this.db.close();
	}
	
	public boolean executaSQL(String SQL){
		if(SQL=="" || SQL==null){
			return false;
		}
		
		try{
			this.db = this.ctx.openOrCreateDatabase(this.nome_banco, Context.MODE_PRIVATE, null);
			this.db.execSQL(SQL);
			this.db.close();
			return true;
		}catch(Exception e){
			return false;
		}
	}

	public Cursor consultaSQL(String SQL){
		this.db = this.ctx.openOrCreateDatabase(this.nome_banco, Context.MODE_PRIVATE, null);
		return this.db.rawQuery(SQL,null);
	}


}
