package com.example.veiculos;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PessoaListActivity extends Activity {

	public EditText edtBusca;
	public Button btnBuscar, btnVoltar;
	public Intent itPrincipal;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pessoa_list);
		
		
		//referenciar edtBusca
		this.edtBusca = (EditText) findViewById(R.id.edtBusca);
		
		//referenciar btnBuscar
		this.btnBuscar = (Button) findViewById(R.id.btnBuscar);
		
		//referenciar btnVoltar
		this.btnVoltar = (Button) findViewById(R.id.btnVoltar);
		
		//referencia Intent LoginActivity
		this.itPrincipal = new Intent(this, PrincipalActivity.class);
	}
	
	public void btnBuscarClick(View v){
		String msg;
		
		msg = "ok";
		
		//feedback pro usuario
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}
	
	public void btnVoltarClick(View v){
		finish();
	}

}
