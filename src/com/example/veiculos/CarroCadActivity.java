package com.example.veiculos;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class CarroCadActivity extends Activity {

	public EditText edtId, edtModelo, edtAno, edtPreco, edtQuilometragem;
	public Spinner spMarca, spPessoa;
	public Button btnGravar, btnVoltar;
	public Intent itPrincipal;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_carro_cad);
		
		//Spinner Marca
		this.spMarca = (Spinner) findViewById(R.id.spMarca);

        this.spMarca = (Spinner) findViewById(R.id.spMarca);
        ArrayAdapter adaptadorMarcas = ArrayAdapter.createFromResource(
                this, R.array.marcas, android.R.layout.simple_spinner_item);
        adaptadorMarcas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spMarca.setAdapter(adaptadorMarcas);
	}
	
	public void btnGravarClick(View v){
		
	}
	
	public void btnVoltarClick(View v){
		finish();
	}
}