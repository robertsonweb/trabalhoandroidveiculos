package com.example.veiculos;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class PrincipalActivity extends Activity {

	public Button btnPessoasCad, btnPessoasList, btnCarrosCad, btnCarrosList, btnVoltar;
	public Intent itLogin, itPessoaCad, itPessoaList, itCarroCad;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principal);
		
		//referenciar btnPessoas
		this.btnPessoasCad = (Button) findViewById(R.id.btnPessoasCad);
		this.btnPessoasList = (Button) findViewById(R.id.btnPessoasList);
		
		//referenciar btnCarros
		this.btnCarrosCad = (Button) findViewById(R.id.btnCarrosCad);
		this.btnCarrosList = (Button) findViewById(R.id.btnCarrosList);
		
		//referenciar btnVoltar
		this.btnVoltar = (Button) findViewById(R.id.btnVoltar);
		
		//referencia Intent LoginActivity
		this.itLogin = new Intent(this, LoginActivity.class);
		
		//referencia Intent PessoaCadActivity
		this.itPessoaCad = new Intent(this, PessoaCadActivity.class);
		
		//referencia Intent PessoaListActivity
		this.itPessoaList = new Intent(this, PessoaListActivity.class);
		
		//referencia Intent CarroCadActivity
		this.itCarroCad = new Intent(this, CarroCadActivity.class);
	}
	
	public void btnPessoasCadClick(View v){
		startActivity(this.itPessoaCad);
	}

	public void btnPessoasListClick(View v){
		startActivity(itPessoaList);
	}
	
	public void btnCarrosCadClick(View v){
		startActivity(this.itCarroCad);
	}

	public void btnCarrosListClick(View v){

	}
	
	public void btnVoltarClick(View v){
		finish();
	}

}
