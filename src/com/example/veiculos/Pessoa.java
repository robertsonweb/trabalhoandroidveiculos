package com.example.veiculos;
import android.database.Cursor;
import android.content.Context;

public class Pessoa {

	private int id;
	private String nome;
	private String login;
	private String senha;
	private Context contexto;
	
	
	public Pessoa(Context contexto) {
		this.contexto 	= contexto;
		this.id			=	0;
		this.nome		=	null;
		this.login		=	null;
		this.senha		=	null;
	}
	
	public Pessoa(Context contexto, int id) {
		this.contexto 	= contexto;
		this.id			=	id;
		this.nome		=	null;
		this.login		=	null;
		this.senha		=	null;
	}
	
	public Pessoa(Context contexto, String login, String senha){
		this.contexto 	= contexto;
		this.id 		=	0;
		this.nome 		=	"";
		this.login 		=	login;
		this.senha 		=	senha;
	}
	
	public Pessoa(Context contexto, int id, String nome, String login, String senha) {
		this.contexto 	= contexto;
		this.id			=	id;
		this.nome		=	nome;
		this.login		=	login;
		this.senha		=	senha;
	}
	
	public int getId(){
		return this.id;
	}
	
	public Boolean gravar(){
		Banco oBanco = new Banco(this.contexto);
		
		if(this.id==0){
			//insert
			return oBanco.executaSQL("INSERT INTO PESSOA (ROWID, NOME, LOGIN, SENHA) VALUES (null,'"+this.nome+"','"+this.login+"','"+this.senha+"')");			
		}else{
			//update
			return oBanco.executaSQL("UPDATE PESSOA SET NOME='"+this.nome+"', LOGIN='"+this.login+"', SENHA='"+this.senha+"' WHERE ID= "+this.id);
		}
	}
	
	public Boolean deletar(){
		return true;
	}
	
	public boolean validarLogin(){
		Banco oBanco = new Banco(this.contexto);
		Cursor ls_retorno = oBanco.consultaSQL("SELECT LOGIN, SENHA FROM PESSOA WHERE LOGIN='"+this.login+"' AND SENHA='"+this.senha+"'");
		
		if(ls_retorno.moveToFirst()){
			oBanco.close();
			return true;
		}
		
		oBanco.close();
		return false;
	}

}
