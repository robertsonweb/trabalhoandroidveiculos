package com.example.veiculos;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

	public EditText edtLogin, edtSenha;
	public Button btnEntrar;
	public Intent itPrincipal;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		//referenciar edtLogin
		this.edtLogin = (EditText) findViewById(R.id.edtLogin);
		
		//referenciar edtLogin
		this.edtSenha = (EditText) findViewById(R.id.edtSenha);
		
		//referenciar btnEntrar
		this.btnEntrar = (Button) findViewById(R.id.btnEntrar);
		
		//Itent para a Activity Principal
		this.itPrincipal = new Intent(this, PrincipalActivity.class);
	}
	
	public void btnEntrarClick(View v){
		String msg;
		
		String login = this.edtLogin.getText().toString();
		String senha = this.edtSenha.getText().toString();
		
		if(login.contentEquals("adm") && senha.contentEquals("adm")){
			msg = "Você está entrando em modo Admin!";
			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
			startActivity(this.itPrincipal);
		}else if(new Pessoa(getApplicationContext(), login, senha).validarLogin()){
			startActivity(this.itPrincipal);
		}else{
			msg = "Usuário ou senha inválidos!";
			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
		}
	}
	
	public void btnSairClick(View v){
		finish();
	}

}
